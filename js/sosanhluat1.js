var dsluatsosanh = [
    {
        id: 1,
		name: 'Đề tài nghiên cứu sơn Nano trong xây dựng',
		datasosanh: [
			{	country: 'Quảng Ninh',
				dstieuchi: [
					{ten: 'Tính Kế Thừa', thongtin: 'Kế thừa sản phẩm thành công hạt Nano sơn chống bám nước, bám bụi, độ bóng bề mặt sơn. Kích thước hạt Nano 150nm'},
					{ten: 'Tính Mới', thongtin: 'Nghiên cứu chế tạo thành công hạt Nano Bạc, có tính năng chống ẩm mốc, chống bay màu sơn, kích thước hạt Nano 100nm'},
					{ten: 'Khả Năng Ứng Dụng Trong Thực Tế', thongtin: 'Do có những tính năng mới chống nấm mốc, chống bay màu sơn và tăng độ móng bề mặt, chống bám nước, bám bụi nên sản phẩm được ứng dụng rỗng rãi trong thực tế'},
					{ten: 'Hiệu Quả Kinh Tế', thongtin: 'Đề tài nghiên cứu đã mang lại những đột phá về công nghệ sơn chống bám nước, bám bụi, bay màu sơn, ẩm mốc. Được khách hàng trong và ngoài nước ưa chuộng, do đó mang lại hiệu quả kinh tế cho tỉnh (hàng triệu USD)'},
					{ten: 'Đánh Giá Đề Tài Nghiên Cứu', thongtin: 'Đề tài được UBND tỉnh và Bộ KHCN đánh giá cao'},
					
				],
			},

			{	country: 'Hà Nội',
				dstieuchi: [
					{ten: 'Tính Kế Thừa', thongtin: 'Nghiên cứu chế tạo thành công hạt Nano kích thước 150nm, có tính năng chống bám nước, bám bụi và tăng độ bóng bề mặt sơn.'},
					{ten: 'Tính Mới', thongtin: ''},
					{ten: 'Khả Năng Ứng Dụng Trong Thực Tế', thongtin: 'Sản phẩm có những tính năng tăng độ móng bề mặt, chống bám nước, bám bụi nên sản phẩm được ứng dụng rỗng rãi trong thực tế'},
					{ten: 'Hiệu Quả Kinh Tế', thongtin: 'Sản phẩm công nghệ sơn Nano có những tính năng chống bám nước, bám bụi. Được khách hàng ưa chuộng, do đó mang lại hiệu quả kinh tế cao'},
					{ten: 'Đánh Giá Đề Tài Nghiên Cứu', thongtin: ''},
					
				],
			},

			{	country: 'HÀ GIANG',
				dstieuchi: [
					{ten: 'Tính Kế Thừa', thongtin: 'Nghiên cứu chế tạo thành công hạt Nano kích thước 150nm, có tính năng chống bám nước, bám bụi và tăng độ bóng bề mặt sơn.'},
					{ten: 'Tính Mới', thongtin: ''},
					{ten: 'Khả Năng Ứng Dụng Trong Thực Tế', thongtin: 'Sản phẩm có những tính năng tăng độ móng bề mặt, chống bám nước, bám bụi nên sản phẩm được ứng dụng rỗng rãi trong thực tế'},
					{ten: 'Hiệu Quả Kinh Tế', thongtin: 'Sản phẩm công nghệ sơn Nano có những tính năng chống bám nước, bám bụi. Được khách hàng ưa chuộng, do đó mang lại hiệu quả kinh tế cao'},
					{ten: 'Đánh Giá Đề Tài Nghiên Cứu', thongtin: ''},
					
				],	
			},

			
			{	country: 'Bắc Ninh',
				dstieuchi: [
					{ten: 'Tính Kế Thừa', thongtin: 'Nghiên cứu chế tạo thành công hạt Nano kích thước 150nm, có tính năng chống bám nước, bám bụi và tăng độ bóng bề mặt sơn.'},
					{ten: 'Tính Mới', thongtin: ''},
					{ten: 'Khả Năng Ứng Dụng Trong Thực Tế', thongtin: 'Sản phẩm có những tính năng tăng độ móng bề mặt, chống bám nước, bám bụi nên sản phẩm được ứng dụng rỗng rãi trong thực tế'},
					{ten: 'Hiệu Quả Kinh Tế', thongtin: 'Sản phẩm công nghệ sơn Nano có những tính năng chống bám nước, bám bụi. Được khách hàng ưa chuộng, do đó mang lại hiệu quả kinh tế cao'},
					{ten: 'Đánh Giá Đề Tài Nghiên Cứu', thongtin: ''},
					
				],	
			},
			
			{	country: 'Quảng Ninh',
				dstieuchi: [
					{ten: 'Tính Kế Thừa', thongtin: 'Nghiên cứu chế tạo thành công hạt Nano kích thước 150nm, có tính năng chống bám nước, bám bụi và tăng độ bóng bề mặt sơn.'},
					{ten: 'Tính Mới', thongtin: ''},
					{ten: 'Khả Năng Ứng Dụng Trong Thực Tế', thongtin: 'Sản phẩm có những tính năng tăng độ móng bề mặt, chống bám nước, bám bụi nên sản phẩm được ứng dụng rỗng rãi trong thực tế'},
					{ten: 'Hiệu Quả Kinh Tế', thongtin: 'Sản phẩm công nghệ sơn Nano có những tính năng chống bám nước, bám bụi. Được khách hàng ưa chuộng, do đó mang lại hiệu quả kinh tế cao'},
					{ten: 'Đánh Giá Đề Tài Nghiên Cứu', thongtin: ''},
				
				],
			},
			{	country: 'Nghệ An',
				dstieuchi: [
					{ten: 'Tính Kế Thừa', thongtin: 'Nghiên cứu chế tạo thành công hạt Nano kích thước 150nm, có tính năng chống bám nước, bám bụi và tăng độ bóng bề mặt sơn.'},
					{ten: 'Tính Mới', thongtin: ''},
					{ten: 'Khả Năng Ứng Dụng Trong Thực Tế', thongtin: 'Sản phẩm có những tính năng tăng độ móng bề mặt, chống bám nước, bám bụi nên sản phẩm được ứng dụng rỗng rãi trong thực tế'},
					{ten: 'Hiệu Quả Kinh Tế', thongtin: 'Sản phẩm công nghệ sơn Nano có những tính năng chống bám nước, bám bụi. Được khách hàng ưa chuộng, do đó mang lại hiệu quả kinh tế cao'},
					{ten: 'Đánh Giá Đề Tài Nghiên Cứu', thongtin: ''},
				
				],	
			},
			{	country: 'Lào Cai',
				dstieuchi: [
					{ten: 'Tính Kế Thừa', thongtin: 'Nghiên cứu chế tạo thành công hạt Nano kích thước 150nm, có tính năng chống bám nước, bám bụi và tăng độ bóng bề mặt sơn.'},
					{ten: 'Tính Mới', thongtin: ''},
					{ten: 'Khả Năng Ứng Dụng Trong Thực Tế', thongtin: 'Sản phẩm có những tính năng tăng độ móng bề mặt, chống bám nước, bám bụi nên sản phẩm được ứng dụng rỗng rãi trong thực tế'},
					{ten: 'Hiệu Quả Kinh Tế', thongtin: 'Sản phẩm công nghệ sơn Nano có những tính năng chống bám nước, bám bụi. Được khách hàng ưa chuộng, do đó mang lại hiệu quả kinh tế cao'},
					{ten: 'Đánh Giá Đề Tài Nghiên Cứu', thongtin: ''},
			
				],	
			},
			{	country: 'Thanh Hóa',
				dstieuchi: [
					{ten: 'Tính Kế Thừa', thongtin: 'Nghiên cứu chế tạo thành công hạt Nano kích thước 150nm, có tính năng chống bám nước, bám bụi và tăng độ bóng bề mặt sơn.'},
					{ten: 'Tính Mới', thongtin: ''},
					{ten: 'Khả Năng Ứng Dụng Trong Thực Tế', thongtin: 'Sản phẩm có những tính năng tăng độ móng bề mặt, chống bám nước, bám bụi nên sản phẩm được ứng dụng rỗng rãi trong thực tế'},
					{ten: 'Hiệu Quả Kinh Tế', thongtin: 'Sản phẩm công nghệ sơn Nano có những tính năng chống bám nước, bám bụi. Được khách hàng ưa chuộng, do đó mang lại hiệu quả kinh tế cao'},
					{ten: 'Đánh Giá Đề Tài Nghiên Cứu', thongtin: ''},
		
				],	
			},
			
		]
    },
    {
        id: 2,
		name: 'Đề tài nghiên cứu sơn Nano trong dược phẩm',
		datasosanh: [
			{	country: 'Quảng Ninh',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: 'Luáº­t ÄÆ¡n vá»‹ hÃ nh chÃ­nh - kinh táº¿ Ä‘áº·c biá»‡t VÃ¢n Äá»“n, Báº¯c VÃ¢n Phong, PhÃº Quá»‘c'},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: 'Quy hoáº¡ch, cÆ¡ cháº¿, chÃ­nh sÃ¡ch Ä‘áº·c biá»‡t vá» phÃ¡t triá»ƒn kinh táº¿ - xÃ£ há»™i, tá»• chá»©c vÃ  hoáº¡t Ä‘á»™ng cá»§a chÃ­nh quyá»n Ä‘á»‹a phÆ°Æ¡ng vÃ  cÆ¡ quan khÃ¡c cá»§a NhÃ  nÆ°á»›c.<br>Nhiá»‡m vá»¥, quyá»n háº¡n cá»§a cÃ¡c cÆ¡ quan nhÃ  nÆ°á»›c á»Ÿ trung Æ°Æ¡ng vÃ  chÃ­nh quyá»n Ä‘á»‹a phÆ°Æ¡ng á»Ÿ tá»‰nh Ä‘á»‘i vá»›i Ä‘Æ¡n vá»‹ hÃ nh chÃ­nh - kinh táº¿ Ä‘áº·c biá»‡t.'},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: 'ÄÆ¡n vá»‹ hÃ nh chÃ­nh - kinh táº¿ Ä‘áº·c biá»‡t VÃ¢n Äá»“n, Báº¯c VÃ¢n Phong, PhÃº Quá»‘c.'},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: ''},
					{ten: 'Ná»™i dung Ä‘iá»u khoáº£n- Qui Ä‘á»‹nh chung', thongtin: ''},
			
				],
			},

			{	country: 'Trung Quốc',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: ''},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: ''},
					{ten: 'Ná»™i dung Ä‘iá»u khoáº£n- Qui Ä‘á»‹nh chung', thongtin: ''},
				
				],
			},

			{	country: 'Mỹ',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: ''},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: ''},
					{ten: 'Ná»™i dung Ä‘iá»u khoáº£n- Qui Ä‘á»‹nh chung', thongtin: ''},
	
				],	
			},

			
			{	country: 'Nhật Bản',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: 'Luáº­t Äáº·c khu chiáº¿n lÆ°á»£c quá»‘c gia'},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: 'Quy hoáº¡ch quá»‘c gia, quy hoáº¡ch Ä‘á»‹a phÆ°Æ¡ng, cÆ¡ cháº¿, chÃ­nh sÃ¡ch Ä‘áº·c biá»‡t vá» Ä‘á»•i má»›i cÆ¡ cáº¥u kinh táº¿ -xÃ£ há»™i liÃªn quan Ä‘áº¿n Äáº·c khu.'},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: 'Táº¥t cáº£ cÃ¡c Äáº·c khu chiáº¿n lÆ°á»£c quá»‘c gia Ä‘Æ°á»£c ChÃ­nh phá»§ chá»‰ Ä‘á»‹nh'},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: ''},
					{ten: 'Ná»™i dung Ä‘iá»u khoáº£n- Qui Ä‘á»‹nh chung', thongtin: ''},
					
				],	
			},
			
			{	country: 'Nga',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: 'Luáº­t liÃªn bang vá» Ä‘áº·c khu kinh táº¿'},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: 'Luáº­t LiÃªn bang, cÃ¡c Nghá»‹ Ä‘á»‹nh cá»§a Chá»§ tá»‹ch LiÃªn bang Nga, cÃ¡c quyáº¿t Ä‘á»‹nh cá»§a ChÃ­nh phá»§ LiÃªn bang Nga vÃ  cÃ¡c hÃ nh vi phÃ¡p lÃ½ quy pháº¡m khÃ¡c Ä‘Æ°á»£c Ã¡p dá»¥ng phÃ¹ há»£p'},
					{ten: 'Ná»™i dung Ä‘iá»u khoáº£n- Qui Ä‘á»‹nh chung', thongtin: ''},
				
				],
			},
			{	country: 'Đức',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: ''},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: ''},
					{ten: 'Ná»™i dung Ä‘iá»u khoáº£n- Qui Ä‘á»‹nh chung', thongtin: ''},
				
				],	
			}
		]
    },
    {
        id: 3,
		name: 'Đề tài nghiên cứu sơn Nano trong xây dựng trong mỹ phẩm',
		datasosanh: [
			{	country: 'Quảng Ninh',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: 'Luáº­t PhÃ²ng, chá»‘ng tham nhÅ©ng'},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: 'Quy Ä‘á»‹nh vá» phÃ²ng ngá»«a, phÃ¡t hiá»‡n, xá»­ lÃ½ tham nhÅ©ng; xá»­ lÃ½ vi pháº¡m phÃ¡p luáº­t vá» phÃ²ng, chá»‘ng tham nhÅ©ng.'},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: '1. Tham Ã´ tÃ i sáº£n.<br>\
					2. Nháº­n há»‘i lá»™.<br>\
					3. Láº¡m dá»¥ng chá»©c vá»¥, quyá»n háº¡n chiáº¿m Ä‘oáº¡t tÃ i sáº£n.<br>\
					4. Lá»£i dá»¥ng chá»©c vá»¥, quyá»n háº¡n trong khi thi hÃ nh nhiá»‡m vá»¥, cÃ´ng vá»¥ vÃ¬ vá»¥ lá»£i.<br>\
					5. Láº¡m quyá»n trong khi thi hÃ nh nhiá»‡m vá»¥, cÃ´ng vá»¥ vÃ¬ vá»¥ lá»£i.<br>\
					6. Lá»£i dá»¥ng chá»©c vá»¥, quyá»n háº¡n gÃ¢y áº£nh hÆ°á»Ÿng Ä‘á»‘i vá»›i ngÆ°á»i khÃ¡c Ä‘á»ƒ trá»¥c lá»£i.<br>\
					7. Giáº£ máº¡o trong cÃ´ng tÃ¡c vÃ¬ vá»¥ lá»£i.<br>\
					8. ÄÆ°a há»‘i lá»™, mÃ´i giá»›i há»‘i lá»™ Ä‘Æ°á»£c thá»±c hiá»‡n bá»Ÿi ngÆ°á»i cÃ³ chá»©c vá»¥, quyá»n háº¡n Ä‘á»ƒ giáº£i quyáº¿t cÃ´ng viá»‡c cá»§a cÆ¡ quan, tá»• chá»©c, Ä‘Æ¡n vá»‹ hoáº·c Ä‘á»‹a phÆ°Æ¡ng vÃ¬ vá»¥ lá»£i.<br>\
					9. Lá»£i dá»¥ng chá»©c vá»¥, quyá»n háº¡n sá»­ dá»¥ng trÃ¡i phÃ©p tÃ i sáº£n vÃ¬ vá»¥ lá»£i.<br>\
					10. NhÅ©ng nhiá»…u vÃ¬ vá»¥ lá»£i.<br>\
					11. KhÃ´ng thá»±c hiá»‡n, thá»±c hiá»‡n khÃ´ng Ä‘Ãºng hoáº·c khÃ´ng Ä‘áº§y Ä‘á»§ nhiá»‡m vá»¥, cÃ´ng vá»¥ vÃ¬ vá»¥ lá»£i.<br>\
					12. Lá»£i dá»¥ng chá»©c vá»¥, quyá»n háº¡n Ä‘á»ƒ bao che cho ngÆ°á»i cÃ³ hÃ nh vi vi pháº¡m phÃ¡p luáº­t vÃ¬ vá»¥ lá»£i; cáº£n trá»Ÿ, can thiá»‡p trÃ¡i phÃ¡p luáº­t vÃ o viá»‡c kiá»ƒm tra, thanh tra, kiá»ƒm toÃ¡n, Ä‘iá»u tra, truy tá»‘, xÃ©t xá»­, thi hÃ nh Ã¡n vÃ¬ vá»¥ lá»£i.'},
					
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: '11 ChÆ°Æ¡ng, 96 Äiá»u'},
					{ten: 'HÃ¬nh pháº¡t Ã¡p dá»¥ng', thongtin: ' Xá»­ lÃ½ cÃ¡n bá»™, cÃ´ng chá»©c, viÃªn chá»©c vi pháº¡m quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t vá» phÃ²ng, chá»‘ng tham nhÅ©ng<br>\
					<br>\
					1. CÃ¡c hÃ nh vi vi pháº¡m quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t vá» phÃ²ng, chá»‘ng tham nhÅ©ng bá»‹ xá»­ lÃ½ bao gá»“m:<br>\
					a) Vi pháº¡m quy Ä‘á»‹nh vá» cÃ´ng khai, minh báº¡ch trong hoáº¡t Ä‘á»™ng cá»§a cÆ¡ quan, tá»• chá»©c, Ä‘Æ¡n vá»‹;<br>\
					b) Vi pháº¡m quy Ä‘á»‹nh vá» Ä‘á»‹nh má»©c, tiÃªu chuáº©n, cháº¿ Ä‘á»™;<br>\
					c) Vi pháº¡m quy Ä‘á»‹nh vá» quy táº¯c á»©ng xá»­;<br>\
					d) Vi pháº¡m quy Ä‘á»‹nh vá» xung Ä‘á»™t lá»£i Ã­ch;<br>\
					Ä‘) Vi pháº¡m quy Ä‘á»‹nh vá» chuyá»ƒn Ä‘á»•i vá»‹ trÃ­ cÃ´ng tÃ¡c cá»§a cÃ¡n bá»™, cÃ´ng chá»©c, viÃªn chá»©c;<br>\
					e) Vi pháº¡m trong hoáº¡t thanh tra, kiá»ƒm toÃ¡n quy Ä‘á»‹nh táº¡i Äiá»u 67 cá»§a Luáº­t nÃ y;<br>\
					g) Vi pháº¡m quy Ä‘á»‹nh vá» nghÄ©a vá»¥ bÃ¡o cÃ¡o vá» hÃ nh vi tham nhÅ©ng vÃ  xá»­ lÃ½ bÃ¡o cÃ¡o vá» hÃ nh vi tham nhÅ©ng;<br>\
					h) Vi pháº¡m quy Ä‘á»‹nh vá» thá»i háº¡n kÃª khai trong kiá»ƒm soÃ¡t tÃ i sáº£n, thu nháº­p;<br>\
					i) Vi pháº¡m quy Ä‘á»‹nh vá» kÃª khai tÃ i sáº£n, thu nháº­p vÃ  hÃ nh vi khÃ¡c vi pháº¡m quy Ä‘á»‹nh vá» kiá»ƒm soÃ¡t tÃ i sáº£n, thu nháº­p.<br>\
					2. TÃ¹y theo tÃ­nh cháº¥t, má»©c Ä‘á»™ vi pháº¡m, cÃ¡c hÃ nh vi quy Ä‘á»‹nh táº¡i khoáº£n 1 Äiá»u nÃ y pháº£i bá»‹ xá»­ lÃ½ ká»· luáº­t hoáº·c bá»‹ truy cá»©u trÃ¡ch nhiá»‡m hÃ¬nh sá»± theo quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t; náº¿u gÃ¢y thiá»‡t háº¡i thÃ¬ pháº£i bá»“i thÆ°á»ng.<br>\
					3. NgÆ°á»i cÃ³ hÃ nh vi vi pháº¡m bá»‹ xá»­ lÃ½ ká»· luáº­t náº¿u lÃ  ngÆ°á»i Ä‘á»©ng Ä‘áº§u hoáº·c cáº¥p phÃ³ cá»§a ngÆ°á»i Ä‘á»©ng Ä‘áº§u cÆ¡ quan, tá»• chá»©c, Ä‘Æ¡n vá»‹ thÃ¬ bá»‹ Ã¡p dá»¥ng tÄƒng trÃ¡ch nhiá»‡m ká»· luáº­t.<br>\
					NgÆ°á»i cÃ³ hÃ nh vi vi pháº¡m bá»‹ xá»­ lÃ½ ká»· luáº­t náº¿u lÃ  thÃ nh viÃªn cá»§a tá»• chá»©c chÃ­nh trá»‹, tá»• chá»©c chÃ­nh trá»‹ - xÃ£ há»™i, tá»• chá»©c xÃ£ há»™i thÃ¬ cÃ²n bá»‹ xá»­ lÃ½ theo Ä‘iá»u lá»‡, quy cháº¿ cá»§a tá»• chá»©c Ä‘Ã³. <br>\
					4. ChÃ­nh phá»§ quy Ä‘á»‹nh cá»¥ thá»ƒ viá»‡c xá»­ lÃ½ ká»· luáº­t, xá»­ pháº¡t hÃ nh chÃ­nh Ä‘á»‘i vá»›i hÃ nh vi pháº¡m quy Ä‘á»‹nh táº¡i khoáº£n 1 Äiá»u nÃ y.<br>\
					Äiá»u 94. Xá»­ lÃ½ hÃ nh vi vi pháº¡m quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t vá» phÃ²ng, chá»‘ng tham nhÅ©ng trong doanh nghiá»‡p, tá»• chá»©c xÃ£ há»™i khu vá»±c ngoÃ i nhÃ  nÆ°á»›c<br>\
					<br>\
					1. NgÆ°á»i giá»¯ chá»©c vá»¥ quáº£n lÃ½ trong cÃ´ng ty Ä‘áº¡i chÃºng, tá»• chá»©c tÃ­n dá»¥ng vÃ  trong tá»• chá»©c xÃ£ há»™i do Thá»§ tÆ°á»›ng ChÃ­nh phá»§, Bá»™ trÆ°á»Ÿng Bá»™ Ná»™i vá»¥, Chá»§ tá»‹ch á»¦y ban nhÃ¢n dÃ¢n cáº¥p tá»‰nh quyáº¿t Ä‘á»‹nh thÃ nh láº­p hoáº·c phÃª duyá»‡t Ä‘iá»u lá»‡ cÃ³ chá»©c nÄƒng huy Ä‘á»™ng cÃ¡c khoáº£n Ä‘Ã³ng gÃ³p cá»§a nhÃ¢n dÃ¢n Ä‘á»ƒ hoáº¡t Ä‘á»™ng tá»« thiá»‡n khu vá»±c ngoÃ i nhÃ  nÆ°á»›c vi pháº¡m cÃ¡c quy Ä‘á»‹nh táº¡i khoáº£n 1 Äiá»u 85 cá»§a Luáº­t nÃ y thÃ¬ bá»‹ xá»­ lÃ½ theo Äiá»u lá»‡, quy cháº¿ hoáº¡t Ä‘á»™ng cá»§a cÃ¡c doanh nghiá»‡p, tá»• chá»©c Ä‘Ã³.<br>\
					2. TrÆ°á»ng há»£p doanh nghiá»‡p, tá»• chá»©c quy Ä‘á»‹nh táº¡i khoáº£n 1 Äiá»u nÃ y khÃ´ng thá»±c hiá»‡n cÃ¡c biá»‡n phÃ¡p xá»­ lÃ½ Ä‘á»‘i vá»›i ngÆ°á»i giá»¯ chá»©c vá»¥ quáº£n lÃ½ thÃ¬ bá»‹ cÆ¡ quan cÃ³ tháº©m quyá»n thanh tra cÃ´ng bá»‘ cÃ´ng khai vá» tÃªn, Ä‘á»‹a chá»‰ vÃ  hÃ nh vi vi pháº¡m theo quy Ä‘á»‹nh cá»§a phÃ¡p luáº­t.'},
					
				],
			},

			{	country: 'Trung Quốc',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: 'Luáº­t hÃ¬nh sá»±'},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: 'Quy Ä‘á»‹nh vá» phÃ²ng ngá»«a, phÃ¡t hiá»‡n, xá»­ lÃ½ tham nhÅ©ng; xá»­ lÃ½ vi pháº¡m phÃ¡p luáº­t vá» phÃ²ng, chá»‘ng tham nhÅ©ng.'},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: '1. Tham Ã´ tÃ i sáº£n.<br>\
					2. Nháº­n há»‘i lá»™.<br>\
					3. Nháº­n há»‘i lá»™ khi báº§u cá»­<br>\
					4. Láº¡m dá»¥ng chá»©c vá»¥, quyá»n háº¡n chiáº¿m Ä‘oáº¡t tÃ i sáº£n.<br>\
					5. Lá»£i dá»¥ng chá»©c vá»¥, quyá»n háº¡n trong khi thi hÃ nh nhiá»‡m vá»¥, cÃ´ng vá»¥ vÃ¬ vá»¥ lá»£i.<br>\
					6. Láº¡m quyá»n trong khi thi hÃ nh nhiá»‡m vá»¥, cÃ´ng vá»¥ vÃ¬ vá»¥ lá»£i.<br>\
					7. Lá»£i dá»¥ng chá»©c vá»¥, quyá»n háº¡n gÃ¢y áº£nh hÆ°á»Ÿng Ä‘á»‘i vá»›i ngÆ°á»i khÃ¡c Ä‘á»ƒ trá»¥c lá»£i.<br>\
					8. Giáº£ máº¡o trong cÃ´ng tÃ¡c vÃ¬ vá»¥ lá»£i.<br>\
					9. ÄÆ°a há»‘i lá»™, mÃ´i giá»›i há»‘i lá»™ Ä‘Æ°á»£c thá»±c hiá»‡n bá»Ÿi ngÆ°á»i cÃ³ chá»©c vá»¥, quyá»n háº¡n Ä‘á»ƒ giáº£i quyáº¿t cÃ´ng viá»‡c cá»§a cÆ¡ quan, tá»• chá»©c, Ä‘Æ¡n vá»‹ hoáº·c Ä‘á»‹a phÆ°Æ¡ng vÃ¬ vá»¥ lá»£i.<br>\
					10. Lá»£i dá»¥ng chá»©c vá»¥, quyá»n háº¡n sá»­ dá»¥ng trÃ¡i phÃ©p tÃ i sáº£n vÃ¬ vá»¥ lá»£i.<br>\
					11. NhÅ©ng nhiá»…u vÃ¬ vá»¥ lá»£i.<br>\
					12. KhÃ´ng thá»±c hiá»‡n, thá»±c hiá»‡n khÃ´ng Ä‘Ãºng hoáº·c khÃ´ng Ä‘áº§y Ä‘á»§ nhiá»‡m vá»¥, cÃ´ng vá»¥ vÃ¬ vá»¥ lá»£i.<br>\
					13. Lá»£i dá»¥ng chá»©c vá»¥, quyá»n háº¡n Ä‘á»ƒ bao che cho ngÆ°á»i cÃ³ hÃ nh vi vi pháº¡m phÃ¡p luáº­t vÃ¬ vá»¥ lá»£i; cáº£n trá»Ÿ, can thiá»‡p trÃ¡i phÃ¡p luáº­t vÃ o viá»‡c kiá»ƒm tra, thanh tra, kiá»ƒm toÃ¡n, Ä‘iá»u tra, truy tá»‘, xÃ©t xá»­, thi hÃ nh Ã¡n vÃ¬ vá»¥ lá»£i.<br>\
					14. Äá»“ng pháº¡m trong cÃ¡c tá»™i trÃªn<br>\
					15. Giáº£ máº¡o báº±ng chá»©ng'},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: '15 chÆ°Æ¡ng, 452 Ä‘iá»u'},
					{ten: 'HÃ¬nh pháº¡t Ã¡p dá»¥ng', thongtin: '1. Há»‘i lá»™:<br>\
					- TÆ°Æ¡ng Ä‘á»‘i lá»›n: pháº¡t dÆ°á»›i 3 nÄƒm tÃ¹ hoáº·c táº¡m giam hÃ¬nh sá»±<br>\
					- Ráº¥t lá»›n: pháº¡t tá»« 3 - 10 nÄƒm tÃ¹, Ä‘á»“ng thá»i pháº¡t tiá»n.<br>\
					+ Náº¿u lÃ  Ä‘Æ¡n vá»‹ vi pháº¡m, Ä‘Æ¡n vá»‹ sáº½ bá»‹ pháº¡t tiá»n vÃ  ngÆ°á»i trá»±c tiáº¿p chá»‹u trÃ¡ch nhiá»‡m sáº½ chá»‹u hÃ¬nh pháº¡t nhÆ° trÃªn.<br>\
					+ Náº¿u chá»§ Ä‘á»™ng thÃº nháº­n vá» hÃ nh vi trÆ°á»›c khi bá»‹ truy tá»‘ sáº½ Ä‘Æ°á»£c giáº£m Ã¡n hoáº·c miá»…n Ã¡n.<br>\
					- Pháº¡t tÆ°Æ¡ng á»©ng theo cÃ¡c má»©c trong pháº§n Tham Ã´ tÃ i sáº£n, náº¿u Ã©p buá»™c há»‘i lá»™ sáº½ bá»‹ pháº¡t náº·ng hÆ¡n.<br>\
					- Náº¿u bá»‹ cÆ°á»¡ng Ã©p Ä‘Æ°a tÃ i sáº£n cho má»™t cÃ´ng chá»©c nhÃ  nÆ°á»›c vÃ  khÃ´ng cÃ³ lá»£i nhuáº­n báº¥t há»£p phÃ¡p sáº½ khÃ´ng bá»‹ coi lÃ  há»‘i lá»™.<br>\
					- Báº¥t ká»³ cÃ¡ nhÃ¢n nÃ o kiáº¿m lá»£i Ã­ch trÃ¡i phÃ¡p luáº­t báº±ng cÃ¡ch há»‘i lá»™, náº¿u tÃ¬nh tiáº¿t nghiÃªm trá»ng hoáº·c gÃ¢y ra tá»•n tháº¥t náº·ng ná» cho lá»£i Ã­ch cá»§a nhÃ  nÆ°á»›c thÃ¬ bá»‹ pháº¡t tÃ¹ tá»‘i Ä‘a khÃ´ng quÃ¡ nÄƒm nÄƒm vÃ  khÃ´ng quÃ¡ mÆ°á»i nÄƒm. TrÆ°á»ng há»£p Ä‘áº·c biá»‡t nghiÃªm trá»ng sáº½ bá»‹ pháº¡t tÃ¹ tá»‘i Ä‘a khÃ´ng dÆ°á»›i mÆ°á»i nÄƒm hoáº·c tÃ¹ chung thÃ¢n, Ä‘á»“ng thá»i bá»‹ tá»‹ch thu tÃ i sáº£n.<br>\
					- Há»‘i lá»™ dÆ°á»›i nhiá»u tÃªn khÃ¡c nhau: bá»‹ pháº¡t tÃ¹ tá»‘i Ä‘a khÃ´ng quÃ¡ ba nÄƒm hoáº·c bá»‹ táº¡m giam hÃ¬nh sá»±. Náº¿u má»™t Ä‘Æ¡n vá»‹ pháº¡m tá»™i trÃªn, Ä‘Æ¡n vá»‹ sáº½ bá»‹ pháº¡t tiá»n vÃ  ngÆ°á»i trá»±c tiáº¿p phá»¥ trÃ¡ch vÃ  nhá»¯ng ngÆ°á»i khÃ¡c chá»‹u trÃ¡ch nhiá»‡m trá»±c tiáº¿p vá» tá»™i Ä‘Ã³ sáº½ bá»‹ xá»­ pháº¡t theo quy Ä‘á»‹nh.<br>\
					- MÃ´i giá»›i há»‘i lá»™ cho má»™t cÃ´ng chá»©c nhÃ  nÆ°á»›c, náº¿u tÃ¬nh tiáº¿t nghiÃªm trá»ng, sáº½ bá»‹ káº¿t Ã¡n tÃ¹ giam khÃ´ng quÃ¡ ba nÄƒm hoáº·c bá»‹ giam giá»¯ hÃ¬nh sá»±. NgÆ°á»i mÃ´i giá»›i há»‘i lá»™, náº¿u tá»± thÃº trÆ°á»›c khi bá»‹ truy tá»‘, sáº½ Ä‘Æ°á»£c giáº£m Ã¡n hoáº·c miá»…n pháº¡t.<br>\
					2. Há»‘i lá»™ trong báº§u cá»­:<br>\
					- PhÃ¡ hoáº¡i báº§u cá»­, ngÄƒn cáº£n cá»­ tri, giáº£ máº¡o há»“ sÆ¡, bÃ¡o cÃ¡o sai sá»‘ phiáº¿u báº§u, trong trÆ°á»ng há»£p nghiÃªm trá»ng, pháº¡t tÃ¹ tá»‘i Ä‘a khÃ´ng quÃ¡ ba nÄƒm, hÃ¬nh sá»± giam giá»¯, tÆ°á»›c quyá»n chÃ­nh trá»‹.<br>\
					3. Giáº£ máº¡o báº±ng chá»©ng<br>\
					- Cáº£n trá»Ÿ cung cáº¥p báº±ng chá»©ng, giáº£ máº¡o hoáº·c tiÃªu há»§y báº±ng chá»©ng: pháº¡t tÃ¹ giam khÃ´ng quÃ¡ 3 nÄƒm hoáº·c táº¡m giam hÃ¬nh sá»±. Náº¿u nghiÃªm trá»ng, pháº¡t tÃ¹ giam tá»« 3 - 7 nÄƒm.<br>\
					- Äáº¡i diá»‡n tÆ° phÃ¡p vi pháº¡m tá»™i trÃªn sáº½ bá»‹ pháº¡t náº·ng hÆ¡n.<br>\
					4. Tham Ã´ tÃ i sáº£n:<br>\
					+ TrÃªn 100.000 nhÃ¢n dÃ¢n tá»‡: pháº¡t tÃ¹ khÃ´ng dÆ°á»›i 10 nÄƒm hoáº·c chung thÃ¢n, Ä‘á»“ng thá»i cÃ³ thá»ƒ bá»‹ tá»‹ch thu tÃ i sáº£n. Äáº·c biá»‡t nghiÃªm trá»ng cÃ³ thá»ƒ bá»‹ tá»­ hÃ¬nh vÃ  tá»‹ch thu tÃ i sáº£n.<br>\
					+ 50.000 - 100.000 nhÃ¢n dÃ¢n tá»‡: pháº¡t tÃ¹ khÃ´ng dÆ°á»›i 5 nÄƒm vÃ  cÃ³ thá»ƒ bá»‹ tá»‹ch thu tÃ i sáº£n. Äáº·c biá»‡t nghiÃªm trá»ng cÃ³ thá»ƒ bá»‹ tÃ¹ chung thÃ¢n vÃ  tá»‹ch thu tÃ i sáº£n.<br>\
					+ 5.000 - 50.000 nhÃ¢n dÃ¢n tá»‡: pháº¡t tÃ¹ 1 - 7 nÄƒm. Náº¿u nghiÃªm trá»ng, pháº¡t tÃ¹ 7 - 10 nÄƒm<br>\
					+ 5.000 - 10.000 nhÃ¢n dÃ¢n tá»‡: cÃ³ dáº¥u hiá»‡u Äƒn nÄƒn vÃ  tá»« bá» tiá»n tham Ã´ cÃ³ thá»ƒ Ä‘Æ°á»£c giáº£m Ã¡n hoáº·c miá»…n Ã¡n, nhÆ°ng sáº½ bá»‹ xá»­ pháº¡t ká»· luáº­t bá»Ÿi Ä‘Æ¡n vá»‹ cá»§a mÃ¬nh hoáº·c cáº¥p cÃ³ tháº©m quyá»n á»Ÿ cáº¥p cao hÆ¡n.<br>\
					+ DÆ°á»›i 5.000 nhÃ¢n dÃ¢n tá»‡: náº¿u tÆ°Æ¡ng Ä‘á»‘i nghiÃªm trá»ng, pháº¡t tÃ¹ khÃ´ng quÃ¡ 2 nÄƒm hoáº·c táº¡m giam cá»‘ Ä‘á»‹nh. TrÆ°á»ng há»£p tÆ°Æ¡ng Ä‘á»‘i nháº¹, thÃ¬ bá»‹ xá»­ pháº¡t ká»· luáº­t theo hoÃ n cáº£nh cá»§a Ä‘Æ¡n vá»‹ mÃ¬nh hoáº·c cáº¥p cÃ³ tháº©m quyá»n á»Ÿ cáº¥p trÃªn.<br>\
					+ Náº¿u liÃªn tá»¥c pháº¡m tá»™i tham Ã´ vÃ  chÆ°a bá»‹ trá»«ng pháº¡t sáº½ bá»‹ trá»«ng pháº¡t theo sá»‘ tiá»n tÃ­ch lÅ©y mÃ  anh ta Ä‘Ã£ tham Ã´.<br>\
					5. Chiáº¿m dá»¥ng quá»¹ cÃ´ng:<br>\
					- CÃ´ng chá»©c bá»‹ pháº¡t tÃ¹ tá»‘i Ä‘a khÃ´ng quÃ¡ nÄƒm nÄƒm hoáº·c bá»‹ táº¡m giam hÃ¬nh sá»±. Náº¿u trÆ°á»ng há»£p nghiÃªm trá»ng thÃ¬ bá»‹ pháº¡t tÃ¹ tá»‘i Ä‘a khÃ´ng dÆ°á»›i nÄƒm nÄƒm. Náº¿u cÃ¡ nhÃ¢n Ä‘Ã³ chiáº¿m Ä‘oáº¡t má»™t lÆ°á»£ng lá»›n quá»¹ cÃ´ng vÃ  khÃ´ng thá»ƒ hoÃ n tráº£, thÃ¬ bá»‹ pháº¡t tÃ¹ tá»‘i Ä‘a khÃ´ng dÆ°á»›i mÆ°á»i nÄƒm hoáº·c tÃ¹ chung thÃ¢n.<br>\
					- Báº¥t ká»³ cÃ¡ nhÃ¢n nÃ o chiáº¿m Ä‘oáº¡t tiá»n vÃ  nguyÃªn váº­t liá»‡u Ä‘Æ°á»£c phÃ¢n bá»• Ä‘á»ƒ cá»©u trá»£ thiÃªn tai, cá»©u náº¡n kháº©n cáº¥p, phÃ²ng ngá»«a vÃ  kiá»ƒm soÃ¡t lÅ© lá»¥t, chÄƒm sÃ³c ngÆ°á»i tÃ n táº­t vÃ  gia Ä‘Ã¬nh cá»§a cÃ¡c liá»‡t sÄ© vÃ  ngÆ°á»i phá»¥c vá»¥ cÃ¡ch máº¡ng, viá»‡c chÄƒm sÃ³c ngÆ°á»i nghÃ¨o, tÃ¡i Ä‘á»‹nh cÆ° vÃ  cá»©u trá»£ xÃ£ há»™i sá»­ dá»¥ng vÃ o má»¥c Ä‘Ã­ch riÃªng sáº½ nháº­n má»™t hÃ¬nh pháº¡t náº·ng hÆ¡n.<br>\
					6. Tá»‘ng tiá»n:<br>\
					- Náº¿u tÃ¬nh tiáº¿t nghiÃªm trá»ng thÃ¬ Ä‘Æ¡n vá»‹ sáº½ bá»‹ pháº¡t tiá»n vÃ  ngÆ°á»i trá»±c tiáº¿p phá»¥ trÃ¡ch vÃ  nhá»¯ng ngÆ°á»i khÃ¡c trá»±c tiáº¿p chá»‹u trÃ¡ch nhiá»‡m vá» tá»™i Ä‘Ã³ sáº½ bá»‹ pháº¡t tÃ¹ tá»‘i Ä‘a khÃ´ng quÃ¡ nÄƒm nÄƒm hoáº·c bá»‹ táº¡m giam hÃ¬nh sá»±<br>\
					'},
					
				],
			},

			{	country: 'Mỹ',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: ''},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: ''},
					{ten: 'HÃ¬nh pháº¡t Ã¡p dá»¥ng', thongtin: ''},
				
				],	
			},

			
			{	country: 'Nhật Bản',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: 'Luáº­t hÃ¬nh sá»±'},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: 'Quy Ä‘á»‹nh vá» phÃ²ng ngá»«a, phÃ¡t hiá»‡n, xá»­ lÃ½ tham nhÅ©ng; xá»­ lÃ½ vi pháº¡m phÃ¡p luáº­t vá» phÃ²ng, chá»‘ng tham nhÅ©ng.'},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: '1.Tham Ã´<br>\
					2. Tham Ã´ liÃªn quan cÃ¡c hoáº¡t Ä‘á»™ng xÃ£ há»™i<br>\
					3. Tham Ã´ tÃ i sáº£n bá»‹ máº¥t'},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: '40 chÆ°Æ¡ng, 264 Ä‘iá»u'},
					{ten: 'HÃ¬nh pháº¡t Ã¡p dá»¥ng', thongtin: '1. Tham Ã´<br>\
					- Tham Ã´ tÃ i sáº£n cá»§a ngÆ°á»i khÃ¡c: bá»‹ pháº¡t tÃ¹ vÃ  lao Ä‘á»™ng cÃ´ng Ã­ch dÆ°á»›i 5 nÄƒm.<br>\
					2. Tham Ã´ tÃ i sáº£n liÃªn quan Ä‘áº¿n cÃ¡c hoáº¡t Ä‘á»™ng xÃ£ há»™i: bá»‹ pháº¡t tÃ¹ vÃ  lao Ä‘á»™ng cÃ´ng Ã­ch dÆ°á»›i 10 nÄƒm.<br>\
					3. Tham Ã´ tÃ i sáº£n bá»‹ máº¥t: bá»‹ pháº¡t tÃ¹ vÃ  lao Ä‘á»™ng cÃ´ng Ã­ch dÆ°á»›i 1 nÄƒm  vÃ  bá»‹ pháº¡t khÃ´ng quÃ¡ 100.000 YÃªn.'},
					
				],	
			},
			
			{	country: 'Nga',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: ''},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: ''},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: ''},
					{ten: 'HÃ¬nh pháº¡t Ã¡p dá»¥ng', thongtin: ''},
				
				],
			},
			{	country: 'Đức',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: 'Äáº¡o luáº­t chá»‘ng tham nhÅ©ng nÆ°á»›c ngoÃ i'},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: 'PhÃ²ng chá»‘ng há»‘i lá»™ trong lÄ©nh vá»±c kinh doanh, áº£nh hÆ°á»Ÿng Ä‘áº¿n quyáº¿t Ä‘á»‹nh cá»§a cÃ´ng chá»©c, khiáº¿n cho cÃ´ng chá»©c thá»±c hiá»‡n hÃ nh vi khÃ´ng Ä‘Ãºng má»±c hoáº·c ngoÃ i tháº©m quyá»n, lá»£i dá»¥ng vá»‹ tháº¿ cá»§a cÃ´ng chá»©c áº£nh hÆ°á»Ÿng Ä‘áº¿n quyáº¿t Ä‘á»‹nh cá»§a cÆ¡ quan nhÃ  nÆ°á»›c,  táº¡o lá»£i tháº¿ trong kinh doanh, áº£nh hÆ°á»Ÿng Ä‘áº¿n chá»‰ Ä‘áº¡o kinh doanh.'},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: 'CÃ¡ nhÃ¢n hoáº·c Ä‘áº¡i diá»‡n cÃ¡c cÆ¡ quan, tá»• chá»©c tham gia hoáº·c cÃ³ liÃªn quan Ä‘áº¿n há»‘i lá»™, nháº­n há»‘i lá»™ trong nÆ°á»›c vÃ  há»‘i lá»™ cÃ´ng chá»©c nÆ°á»›c ngoÃ i.'},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: ''},
					{ten: 'HÃ¬nh pháº¡t Ã¡p dá»¥ng', thongtin: '-PhÃ¡p nhÃ¢n:\
					pháº¡t khÃ´ng quÃ¡ $ 2.000.000\
					pháº¡t dÃ¢n sá»± khÃ´ng quÃ¡ $ 10.000 Ã¡p dá»¥ng trong má»™t vá»¥ kiá»‡n do Bá»™ trÆ°á»Ÿng TÆ° phÃ¡p Ä‘Æ°a ra\
					- CÃ¡ nhÃ¢n khÃ¡c\
					pháº¡t khÃ´ng quÃ¡ $ 100.000\
					hoáº·c bá»‹ giam giá»¯ khÃ´ng quÃ¡ 5 nÄƒm, hoáº·c cáº£ hai.\
					pháº¡t dÃ¢n sá»± khÃ´ng quÃ¡ $ 10,000 Ã¡p dá»¥ng trong má»™t vá»¥ kiá»‡n do Bá»™ trÆ°á»Ÿng TÆ° phÃ¡p Ä‘Æ°a ra'},
			
				],	
			},
			{	country: 'Anh',
				dstieuchi: [
					{ten: 'TÃªn cá»§a Luáº­t', thongtin: 'Äáº¡o luáº­t chá»‘ng há»‘i lá»™ nÄƒm 2010'},
					{ten: 'Pháº¡m vi Ä‘iá»u chá»‰nh', thongtin: 'PhÃ²ng chá»‘ng há»‘i lá»™ trong lÄ©nh vá»±c kinh doanh, áº£nh hÆ°á»Ÿng Ä‘áº¿n quyáº¿t Ä‘á»‹nh cá»§a cÃ´ng chá»©c, khiáº¿n cho cÃ´ng chá»©c thá»±c hiá»‡n hÃ nh vi khÃ´ng Ä‘Ãºng má»±c hoáº·c ngoÃ i tháº©m quyá»n, lá»£i dá»¥ng vá»‹ tháº¿ cá»§a cÃ´ng chá»©c áº£nh hÆ°á»Ÿng Ä‘áº¿n quyáº¿t Ä‘á»‹nh cá»§a cÆ¡ quan nhÃ  nÆ°á»›c,  táº¡o lá»£i tháº¿ trong kinh doanh, áº£nh hÆ°á»Ÿng Ä‘áº¿n chá»‰ Ä‘áº¡o kinh doanh'},
					{ten: 'Äá»‘i tÆ°á»£ng Ä‘iá»u chá»‰nh', thongtin: 'CÃ¡ nhÃ¢n hoáº·c Ä‘áº¡i diá»‡n cÃ¡c cÆ¡ quan, tá»• chá»©c tham gia hoáº·c cÃ³ liÃªn quan Ä‘áº¿n há»‘i lá»™, nháº­n há»‘i lá»™ trong nÆ°á»›c vÃ  há»‘i lá»™ cÃ´ng chá»©c nÆ°á»›c ngoÃ i.'},
					{ten: 'Káº¿t cáº¥u Luáº­t', thongtin: ''},
					{ten: 'HÃ¬nh pháº¡t Ã¡p dá»¥ng', thongtin: 'Pháº¡t tÃ¹ khÃ´ng quÃ¡ 10 nÄƒm, hoáº·c sáº½ bá»‹ pháº¡t tiá»n, hoáº·c cáº£ hai.'},
					
				],
			},
		]
    }
    
]


$(document).ready(function() {

	$('#btn-so-sanh').click(function (e) {

		let tenluat  = $("#example-search-input").val();
		// console.log(tenluat);
		let luatdata = dsluatsosanh.find(item=> item.name == tenluat);
		
		if (luatdata) {
			let nuoc1 = $("#nuoc-thu-1").val();
			let datanuoc1 = luatdata.datasosanh.find(item=> item.country == nuoc1);
			console.log(nuoc1);
			console.log(luatdata.datasosanh);
			let nuoc2 = $("#nuoc-thu-2").val();
			console.log(nuoc2);
			let datanuoc2 = luatdata.datasosanh.find(item=> item.country == nuoc2);
			if (datanuoc1 && datanuoc2) {
				//let keys = Object.getOwnPropertyNames(datanuoc1);
				//console.log(JSON.stringify(datanuoc1.dstieuchi));
				$('#bang-so-sanh').empty();
					
				datanuoc1.dstieuchi.forEach((element, index) => {
					console.log(index);
					$('#bang-so-sanh').append(
						'<button class="btn btn-primary" style="width: 100%; margin-bottom: 5px; background: rgb(30, 70, 125);" data-toggle="collapse" data-target="#table-so-sanh'+ index + '">' + datanuoc1.dstieuchi[index].ten+ '</button>\
						<div id="table-so-sanh'+ index + '" class="collapse">\
							<table class="soSanhLuat" cellpadding="0px" cellspacing="1px" border="none" width="100%">\
								<thead>\
									<tr>\
										<th class="quocgia1" style="width: 50%">'+ nuoc1 +'</th>\
										<th class="quocgia2" style="width: 50%">'+ nuoc2 +'</th>\
									</tr>\
								</thead>\
								<tbody>\
										<tr>\
											<td>'+ datanuoc1.dstieuchi[index].thongtin +'</td>\
											<td>'+ datanuoc2.dstieuchi[index].thongtin +'</td>\
										</tr>\
								</tbody>\
								</table>\
						</div>');
				});
			}
			//console.log(JSON.stringify(datanuoc1));
		}
		//alert ( + $("#nuoc-thu-2").val());
		$('#root').attr('style', 'height: unset !important');
		$('#root').attr('style', 'background: url("imgs/bg-02.png") no-repeat fixed center;')

	});

	var url_string = window.location.href;
	var url = new URL(url_string);
	var quocgia1 = url.searchParams.get("quocgia1");
	var quocgia2 = url.searchParams.get("quocgia2");
			var codeLuat = url.searchParams.get("maLuat");
			//alert(codeLuat);
	$('.breadcrumb-title').html(quocgia1 + ' vá»›i ' + quocgia2);
	
			
	$("#nuoc-thu-1").val(quocgia1);
	$("#nuoc-thu-2").val(quocgia2);
	var luat = dsluatsosanh.find(item => item.id == codeLuat);
	if (luat && quocgia1 && quocgia2) {
		$('.quocgia1').html(quocgia1);
		$('.quocgia2').html(quocgia2);
		$('#example-search-input').val(luat.name);

		$('#btn-so-sanh').trigger('click');
	}
	
});