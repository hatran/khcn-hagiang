﻿$(document).ready(function () {
    $('#cmail2').hide();
    $('#cmail3').hide();
    $('#cmail4').hide();
})
$('#mail1').click(function () {
    $('#cmail1').show();
    $('#cmail2').hide();
    $('#cmail3').hide();
    $('#cmail4').hide();
})
$('#mail2').click(function () {
    $('#cmail2').show();
    $('#cmail1').hide();
    $('#cmail3').hide();
    $('#cmail4').hide();
})
$('#mail3').click(function () {
    $('#cmail3').show();
    $('#cmail1').hide();
    $('#cmail2').hide();
    $('#cmail4').hide();
})
$('#mail4').click(function () {
    $('#cmail4').show();
    $('#cmail1').hide();
    $('#cmail3').hide();
    $('#cmail2').hide();
})

$('.soanthu').click(function () {
    $('#GuiToi').val('');
    $('#TieuDe').val('');
    $('#summernote').summernote('editor.insertText', '');
    $('#ModalSoanThu').modal('show');
})
$('.traloi').click(function () {
    $('#GuiToi').val('Nguyễn Văn A');
    $('#TieuDe').val('Rep: Thông báo kết quả lấy ý kiến về việc chuẩn bị cho việc dự thảo luật phòng chống tham nhũng (sửa đổi) - (dữ liệu Demo). ');
    $('#summernote').summernote('editor.insertText', '');
    $('#ModalSoanThu').modal('show');
})

$('.chuyentiep').click(function () {
    $('#GuiToi').val('Nguyễn Văn A');
    $('#TieuDe').val('FW: Thông báo kết quả lấy ý kiến về việc chuẩn bị cho việc dự thảo luật phòng chống tham nhũng (sửa đổi) - (dữ liệu Demo). ');
    $('#summernote').summernote('editor.insertText', 'Theo kế hoạch, đầu tuần tới, Thường vụ QH sẽ cho ý kiến lần đầu tiên về Dự thảo Luật Phòng, chống tham nhũng (PCTN) sửa đổi - dự kiến sẽ được thông qua tại kỳ họp QH vào tháng 10. Cần phải sửa những gì để đạo luật này thực sự là công cụ pháp lý có hiệu lực cho công cuộc chống tham nhũng tại Việt Nam? Ông Jairo Acuna Alfaro, cố vấn chính sách của UNDP tại Việt Nam đã có bài góp ý về dự án luật này trên Pháp luật TỈNH HÀ GIANG. Xét từ quan điểm chính sách, thách thức của Việt Nam nằm ở nghịch lý rằng đất nước này có một hệ thống chính sách và khung pháp lý chính thống rất toàn diện(ở nhiều cấp độ) song tham nhũng vẫn bị xem là phổ biến và là vấn đề thực tiễn.');
    $('#ModalSoanThu').modal('show');
})

$('.inthu').click(function () {
    var item = $(this).parent().parent().parent().next().next();
    var mywindow = window.open('', 'PRINT', 'height=500,width=1000');

    mywindow.document.write('<html><head><title>' + document.title + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + $(this).parent().parent().next().next().html() + '</h1>');
    mywindow.document.write('<div class="view-mail" style="font-size: 14px;">' + item.html() + '</div>');
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();
})


$('#thudagui').click(function () {
    $('.chomthu').hide();
    $('.cthudagui').show();
})
$('#homthu').click(function () {
    $('.chomthu').show();
})
$('#thunhap').click(function () {
    $('.chomthu').hide();
    $('.cthunhap').show();
})
$('#thudaxoa').click(function () {
    $('.chomthu').hide();
    $('.cthudaxoa').show();
})

$(document).ready(function () {
    $('#node2').addClass('hide');
    $('#node3').addClass('hide');
    $('#node4').addClass('hide');
    $('#node5').addClass('hide');
    $('#node6').addClass('hide');
    $('#node7').addClass('hide');
})
$(document).on('click', '#j1_1_anchor', function () {
    $('#node1').removeClass('hide');
    $('#node2').addClass('hide');
    $('#node3').addClass('hide');
    $('#node4').addClass('hide');
    $('#node5').addClass('hide');
    $('#node6').addClass('hide');
    $('#node7').addClass('hide');
})
$(document).on('click', '#j1_2_anchor', function () {
    $('#node2').removeClass('hide');
    $('#node3').addClass('hide');
    $('#node1').addClass('hide');
    $('#node4').addClass('hide');
    $('#node5').addClass('hide');
    $('#node6').addClass('hide');
    $('#node7').addClass('hide');
})
$(document).on('click', '#j1_3_anchor', function () {
    $('#node3').removeClass('hide');
    $('#node1').addClass('hide');
    $('#node4').addClass('hide');
    $('#node2').addClass('hide');
    $('#node5').addClass('hide');
    $('#node6').addClass('hide');
    $('#node7').addClass('hide');
})
$(document).on('click', '#j1_4_anchor', function () {
    $('#node4').removeClass('hide');
    $('#node2').addClass('hide');
    $('#node3').addClass('hide');
    $('#node1').addClass('hide');
    $('#node5').addClass('hide');
    $('#node6').addClass('hide');
    $('#node7').addClass('hide');
})
$(document).on('click', '#j1_5_anchor', function () {
    $('#node5').removeClass('hide');
    $('#node2').addClass('hide');
    $('#node1').addClass('hide');
    $('#node4').addClass('hide');
    $('#node3').addClass('hide');
    $('#node6').addClass('hide');
    $('#node7').addClass('hide');
})
$(document).on('click', '#j1_6_anchor', function () {
    $('#node6').removeClass('hide');
    $('#node1').addClass('hide');
    $('#node3').addClass('hide');
    $('#node2').addClass('hide');
    $('#node5').addClass('hide');
    $('#node4').addClass('hide');
    $('#node7').addClass('hide');
})
$(document).on('click', '#j1_8_anchor', function () {
    $('#node7').removeClass('hide');
    $('#node1').addClass('hide');
    $('#node3').addClass('hide');
    $('#node2').addClass('hide');
    $('#node5').addClass('hide');
    $('#node6').addClass('hide');
    $('#node4').addClass('hide');
})
