﻿var widthDevice = 986;// width ipad,mobile
// van ban luat check show content with device
$(document).ready(function () {
    InitDevice();
    $(window).on('resize', function () {
        if ($(this).width() >= 769) {
            $('#box-law-left-menu').css('display', 'block')
            $('#box-law-left-menu').css('margin-top', '0px');
        }
        else {
            $('#box-law-left-menu').css('display', 'none')

        }
    });

    if (window.location.pathname.includes("/van-ban")) {
        ActiveLink("coll-area", "list-group-area");
        ActiveLink("coll-doc", "list-group-doc");
    } 

    if (window.location.pathname.includes("/van-ban/")) {
        $('#' + sessionStorage.getItem("classActive")).children().addClass("active");

    } else {
        $('#' + sessionStorage.getItem("classActive")).children().removeClass("active");
        sessionStorage.setItem("classActive", "");
    }
})
InitDevice = function () {
    if ($(this).width() >= 769) {
        $('#box-law-left-menu').css('display', 'block')
        $('#box-law-left-menu').css('margin-top', '0px');
    } else {
        $('#box-law-left-menu').css('display', 'none')

    }
}
// active thể loại và lĩnh vực khi lick
ActiveLink = function (id, lstClass) {
    var header = document.getElementById(id);
    var btns = header.getElementsByClassName(lstClass);
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function () {
            sessionStorage.setItem("classActive", this.id);
        });
    }
}

function togglerBoxLeftMenu(divId) {
    $("#" + divId).toggle();
    $("#" + divId).css('margin-top', $(".navbar-toggle-law-left-menu").offset().top - 16 +'px');
   
}