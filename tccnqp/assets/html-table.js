var DatatableHtmlTableDemo = {
    init: function() {
        var e;
        e = $("#html_table_0").mDatatable({
            data: {
                saveState: {
                    cookie: !1
                }
            },
            search: {
                input: $("#generalSearch")
            },
            columns: [{
                field: "Deposit Paid",
                type: "number"
            }, {
                field: "Order Date",
                type: "date",
                format: "YYYY-MM-DD"
            }, {
                field: "Status",
                class: "hide",
                title: "trạng thái",
                template: function(e) {
                    var t = {
                        1: {
                            title: "Chờ xử lý",
                            class: " hide"
                        },
                        2: {
                            title: "Đang xử lý",
                            class: " hide"
                        },
                        3: {
                            title: "Đã xử lý",
                            class: " hide"
                        },
                        4: {
                            title: "Success",
                            class: " hide"
                        },
                        5: {
                            title: "Info",
                            class: " m-badge--info"
                        },
                        6: {
                            title: "Danger",
                            class: " m-badge--danger"
                        },
                        7: {
                            title: "Warning",
                            class: " m-badge--warning"
                        }
                    };
                    return '<span class="m-badge m-badge--wide"></span>'
                }
            }, {
                field: "Type",
                title: "Type",
                template: function(e) {
                    var t = {
                        1: {
                            title: "Online",
                            state: "danger"
                        },
                        2: {
                            title: "Retail",
                            state: "primary"
                        },
                        3: {
                            title: "Direct",
                            state: "accent"
                        }
                    };
                    return '<span class="m-badge m-badge--' + t[e.Type].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' + t[e.Type].state + '">' + t[e.Type].title + "</span>"
                }
            }]
        }), $("#m_form_status").on("change", function() {
            e.search($(this).val().toLowerCase(), "Status")
        }), $("#m_form_type").on("change", function() {
            e.search($(this).val().toLowerCase(), "Type")
        }), $("#m_form_status, #m_form_type").selectpicker()
    }
};
var DatatableHtmlTableDemo1 = {
    init: function() {
        var e;
        e = $("#html_table_1").mDatatable({
            data: {
                saveState: {
                    cookie: !1
                }
            },
            search: {
                input: $("#generalSearch1")
            },
            columns: [{
                field: "Deposit Paid",
                type: "number"
            }, {
                field: "Order Date",
                type: "date",
                format: "YYYY-MM-DD"
            }, {
                field: "Status",
                title: "trạng thái",
            }, {
                field: "Type",
                title: "Type"
            }]
        })
    }
};
jQuery(document).ready(function() {
    DatatableHtmlTableDemo.init();
    DatatableHtmlTableDemo1.init();
});